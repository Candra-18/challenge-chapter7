import React from 'react';
import "bootstrap/dist/js/bootstrap.min.js";
import "bootstrap/dist/css/bootstrap.min.css";
import { Provider } from "react-redux";
import store from "./redux/store";
import {Routes,BrowserRouter,Route} from "react-router-dom"
import Cars from './pages/Cars';
import MainLayout from './mainlayout/MainLayout';






class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
      <BrowserRouter>
      <Routes>
        <Route path="/" element ={<MainLayout />}/>

        <Route path="/Cars" element ={<Cars />}/>
      </Routes>
    </BrowserRouter>
    </Provider>

    );
  }
}

export default App;

import React from 'react'
import Header from '../components/header/Header'
import OurService from '../components/ourservice/OurService'
import WhyUs from '../components/whyus/WhyUs'
import Carousel from '../components/carousel/Carousel'
import Banner from '../components/banner/Banner'
import FAQ from '../components/faq/Faq'
import Footer from '../components/footer/Footer'


const Index = () => {
    return(
        <div>
        <Header/>
       <OurService/>
       <WhyUs/>
       <Carousel/>
       <Banner/>
       <FAQ />
       <Footer/>
        </div>
    )
}
export default Index
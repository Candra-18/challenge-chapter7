import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";

function  OurService() {
  return (
<section id="our-service" className="pt-5">
  <div className="container">
    <div className="row">
      <div className="col-lg-6 py-4">
        <img src="img\service.png" className="img-fluid img-our" alt="Our Service" />
      </div>
      <div className="col-lg-5 py-4">
        <h2 className="mb-4">Best Car Rental for any kind of trip in Yogyakarta!</h2>
        <p className="mb-4">Sewa mobil di Karawang bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
        <div className="my-1"><img src="img\ceklis.png"  style={{maxWidth: '26px', maxHeight: '30px'}}   className="ceklis mx-2 my-1" />Sewa Mobil Dengan Supir di Bali 12 Jam</div>
        <div className="my-1"><img src="img\ceklis.png"  style={{maxWidth: '26px', maxHeight: '30px'}}   className="ceklis mx-2 my-1" />Sewa Mobil Lepas Kunci di Bali 24 Jam</div>
        <div className="my-1"><img src="img\ceklis.png"  style={{maxWidth: '26px', maxHeight: '30px'}}   className="ceklis mx-2 my-1" />Sewa Mobil Jangka Panjang Bulanan</div>
        <div className="my-1"><img src="img\ceklis.png"  style={{maxWidth: '26px', maxHeight: '30px'}}   className="ceklis mx-2 my-1" />Gratis Antar - Jemput Mobil di Bandara</div>
        <div className="my-1"><img src="img\ceklis.png"  style={{maxWidth: '26px', maxHeight: '30px'}}   className="ceklis mx-2 my-1" />Layanan Airport Transfer / Drop In Out</div>
      </div>
    </div>
  </div>
</section>


);

}

export default OurService;

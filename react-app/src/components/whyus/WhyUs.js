import React from "react";

import "bootstrap/dist/css/bootstrap.min.css";

function  WhyUs() {
  return (
    <section id="why-us" className="pt-5 pb-3">
    <div className="container my-3">
      <h2 className="why-us-text">Why Us?</h2>
      <p className="why-us-text">Mengapa harus pilih Binar Car Rental?</p>
      <div className="row row-cols-1 row-cols-md-4 g-4">
        <div className="col">
          <div className="card card-why-us">
            <div className="card-body">
              <img className="icon mx-2 my-3" src="img\icon_complete.png" alt="" />
              <h5 className="card-title fw-bold p-2">Mobil Lengkap</h5>
              <p className="card-text p-2">
                Tersedia banyak pilihan mobil, kondisi masih baru, bersih
                dan terawat
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card card-why-us">
            <div className="card-body">
              <img className="icon mx-2 my-3" src="img\icon_price.png" alt="" />
              <h5 className="card-title fw-bold p-2">Harga Murah</h5>
              <p className="card-text p-2">
                Harga murah dan bersaing, bisa bandingkan harga kami dengan
                rental mobil lain
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card card-why-us">
            <div className="card-body">
              <img className="icon mx-2 my-3" src="img\icon_24hrs.png" alt="" />
              <h5 className="card-title fw-bold p-2">Layanan 24 Jam</h5>
              <p className="card-text p-2">
                Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami
                juga tersedia di akhir minggu
              </p>
            </div>
          </div>
        </div>
        <div className="col">
          <div className="card card-why-us">
            <div className="card-body">
              <img className="icon mx-2 my-3" src="img\icon_professional.png" alt="" />
              <h5 className="card-title fw-bold p-2">Sopir Profesional</h5>
              <p className="card-text p-2">
                Sopir yang profesional, berpengalaman, jujur, ramah dan
                selalu tepat waktu
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  

);

}

export default WhyUs;